const rootPath = '/root_project';

module.exports = {
    DIR_ROOT: rootPath,
    DIR_WWW: `${rootPath}/www`,
    DEV_SERVER_DEFAULT_PORT: 8090,
    DEV_SERVER_DEFAULT_HOST: '192.168.1.41',
};
