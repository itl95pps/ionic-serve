'use strict';
const path = require('path');
const express = require('express');
const fs = require('fs');
const { DIR_ROOT, DIR_WWW } = require('./config');

const IOS_PLATFORM_PATHS = [path.join('platforms', 'ios', 'www')];
const ANDROID_PLATFORM_PATHS = [
    path.join('platforms', 'android', 'assets', 'www'),
    path.join('platforms', 'android', 'app', 'src', 'main', 'assets', 'www')
];

function createHttpServer() {
    const app = express();
    app.get('/', serveIndex);
    app.use('/', express.static(DIR_WWW));
    app.get('/cordova.js', servePlatformResource, serveMockCordovaJS);
    app.get('/cordova_plugins.js', servePlatformResource);
    app.get('/plugins/*', servePlatformResource);

    return app;
}

function serveIndex(req, res) {
    const indexFileName = path.join(DIR_WWW, 'index.html');
    fs.readFile(indexFileName, function (err, indexHtml) {
        if (!indexHtml) {
            res.send('index.html não encontrado');
            return;
        }
        res.set('Content-Type', 'text/html');
        res.send(indexHtml);
    });
}

function serveMockCordovaJS(req, res) {
    res.set('Content-Type', 'application/javascript');
    res.send('');
}

async function servePlatformResource(req, res, next) {
    const userAgent = req.header('user-agent');

    const root = await getResourcePath(req.url, userAgent);
    if (root) {
        res.sendFile(req.url, { root });
    } else {
        next();
    }
}

async function getResourcePath(url, userAgent) {
    let searchPaths = [DIR_WWW];
    if (isUserAgentIOS(userAgent)) {
        searchPaths = IOS_PLATFORM_PATHS.map(resourcePath => path.join(DIR_ROOT, resourcePath));
    } else if (isUserAgentAndroid(userAgent)) {
        searchPaths = ANDROID_PLATFORM_PATHS.map(resourcePath => path.join(DIR_ROOT, resourcePath));
    }

    for (let i = 0; i < searchPaths.length; i++) {
        const checkPath = path.join(searchPaths[i], url);
        try {
            let result = await checkFile(checkPath);
            return searchPaths[i];
        } catch (e) { }
    }
}

function checkFile(filePath) {
    return new Promise(function (resolve, reject) {
        fs.stat(filePath, function (err, stats) {
            if (err) {
                return reject();
            }
            resolve();
        });
    });
}

function isUserAgentIOS(ua) {
    ua = ua.toLowerCase();
    return (ua.indexOf('iphone') > -1 || ua.indexOf('ipad') > -1 || ua.indexOf('ipod') > -1);
}

function isUserAgentAndroid(ua) {
    ua = ua.toLowerCase();
    return ua.indexOf('android') > -1;
}

exports.createHttpServer = createHttpServer;
