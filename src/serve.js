'use strict';
const http_server = require('./http-server');
const { DEV_SERVER_DEFAULT_PORT, DEV_SERVER_DEFAULT_HOST } = require('./config');

function serve() {
    try {
        http_server.createHttpServer()
            .listen(DEV_SERVER_DEFAULT_PORT, DEV_SERVER_DEFAULT_HOST);
    } catch (error) {
        console.log(error);
    }
}

serve();
